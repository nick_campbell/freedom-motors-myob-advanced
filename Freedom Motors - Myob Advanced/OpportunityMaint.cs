using System;
using System.Collections.Specialized;
using System.Linq;
using PX.Common;
using PX.Data;
using System.Collections;
using PX.Objects.CS;
using PX.Objects.CM;
using PX.Objects.AR;
using PX.Objects.IN;
using PX.Objects.TX;
using PX.Objects.SO;
using System.Collections.Generic;
using System.Diagnostics;
using PX.Objects.GL;
using PX.Objects;
using PX.Objects.CR;

namespace PX.Objects.CR
{

    public class OpportunityMaint_Extension : PXGraphExtension<OpportunityMaint>
    {

        #region Event Handlers

        public PXAction<PX.Objects.CR.CROpportunity> PrintOpportunity;

        [PXButton(CommitChanges = true)]
        [PXUIField(DisplayName = "Print")]
        protected void printOpportunity()
        {
            CROpportunity cROpportunity = Base.Opportunity.Current;

            if (cROpportunity.OpportunityID != null)
            {
                throw new PXReportRequiredException(cROpportunity, "MAWS1000", null);
            }
        }





        #endregion

    }


}