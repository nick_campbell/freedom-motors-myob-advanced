using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using POLine = PX.Objects.PO.POLine;
using POOrder = PX.Objects.PO.POOrder;
using System.Threading.Tasks;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AR.CCPaymentProcessing;
using ARRegisterAlias = PX.Objects.AR.Standalone.ARRegisterAlias;
using PX.Objects.Common;
using PX.Objects.IN.Overrides.INDocumentRelease;
using PX.Objects;
using PX.Objects.SO;

namespace PX.Objects.SO
{

    public class SOOrderEntry_Extension : PXGraphExtension<SOOrderEntry>
    {

        #region Event Handlers

        protected void SOLine_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (SOLine)e.Row;

            if (row != null && row.OrderType == "SA")
            {
                PXDefaultAttribute.SetPersistingCheck<SOLine.reasonCode>(cache, e.Row, PXPersistingCheck.Null);
            }
        }



        protected void SOLine_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            var row = (SOLine)e.Row;

            if (row != null && row.OrderType == "SA" && row.ReasonCode == null)
            {
                String reasonsCodeWarning = "You will need enter a reason code for SA (Services & Parts)";
                throw new PXSetPropertyException(reasonsCodeWarning, PXErrorLevel.Warning);
            }
        }


        #endregion

    }


}