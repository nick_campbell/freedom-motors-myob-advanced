using PX.Common;
using PX.Data.EP;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using PX.Objects;
using PX.TM;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace PX.Objects.CR
{
  public class CROpportunityExt : PXCacheExtension<PX.Objects.CR.CROpportunity>
  {
      #region UsrMAWSJobNumber
      [PXDBString(20)]
      [PXUIField(DisplayName="Job Number")]
      public virtual string UsrMAWSJobNumber { get; set; }
      public abstract class usrMAWSJobNumber : IBqlField { }
      #endregion

      #region UsrMAWSColor
      [PXDBString(20)]
      [PXUIField(DisplayName="Color")]
      public virtual string UsrMAWSColor { get; set; }
      public abstract class usrMAWSColor : IBqlField { }
      #endregion

      #region UsrMAWSMake
      [PXDBString(20)]
      [PXUIField(DisplayName="Make")]
      public virtual string UsrMAWSMake { get; set; }
      public abstract class usrMAWSMake : IBqlField { }
      #endregion

      #region UsrMAWSModel
      [PXDBString(20)]
      [PXUIField(DisplayName="Model")]
      public virtual string UsrMAWSModel { get; set; }
      public abstract class usrMAWSModel : IBqlField { }
      #endregion

      #region UsrMAWSRegistration
      [PXDBString(20)]
      [PXUIField(DisplayName="Registration")]
      public virtual string UsrMAWSRegistration { get; set; }
      public abstract class usrMAWSRegistration : IBqlField { }
      #endregion

      #region UsrMAWSVIN
      [PXDBString(20)]
      [PXUIField(DisplayName="VIN")]
      public virtual string UsrMAWSVIN { get; set; }
      public abstract class usrMAWSVIN : IBqlField { }
      #endregion

      #region UsrMAWSEngineNumber
      [PXDBString(20)]
      [PXUIField(DisplayName="Engine Number")]
      public virtual string UsrMAWSEngineNumber { get; set; }
      public abstract class usrMAWSEngineNumber : IBqlField { }
      #endregion

      #region UsrMAWSDeliveryDate
      [PXDBDate]
      [PXUIField(DisplayName="Delivery Date")]
      public virtual DateTime? UsrMAWSDeliveryDate { get; set; }
      public abstract class usrMAWSDeliveryDate : IBqlField { }
      #endregion

      #region UsrMAWSOption1
      [PXDBString(50)]
      [PXUIField(DisplayName="Option1")]
      public virtual string UsrMAWSOption1 { get; set; }
      public abstract class usrMAWSOption1 : IBqlField { }
      #endregion

      #region UsrMAWSOption2
      [PXDBString(20)]
      [PXUIField(DisplayName="Option2")]
      public virtual string UsrMAWSOption2 { get; set; }
      public abstract class usrMAWSOption2 : IBqlField { }
      #endregion

      #region UsrMAWSOption3
      [PXDBString(50)]
      [PXUIField(DisplayName="Option3")]
      public virtual string UsrMAWSOption3 { get; set; }
      public abstract class usrMAWSOption3 : IBqlField { }
      #endregion

      #region UsrMAWSOption4
      [PXDBString(50)]
      [PXUIField(DisplayName="Option4")]
      public virtual string UsrMAWSOption4 { get; set; }
      public abstract class usrMAWSOption4 : IBqlField { }
      #endregion

      #region UsrMAWSOption5
      [PXDBString(50)]
      [PXUIField(DisplayName="Option5")]
      public virtual string UsrMAWSOption5 { get; set; }
      public abstract class usrMAWSOption5 : IBqlField { }
      #endregion

      #region UsrMAWSTypeOfConversion
      [PXDBString(50)]
      [PXUIField(DisplayName="Type Of Conversion")]
      public virtual string UsrMAWSTypeOfConversion { get; set; }
      public abstract class usrMAWSTypeOfConversion : IBqlField { }
      #endregion

      #region UsrMAWSBirthDate
      [PXDBDate]
      [PXUIField(DisplayName="Birth Date")]
      public virtual DateTime? UsrMAWSBirthDate { get; set; }
      public abstract class usrMAWSBirthDate : IBqlField { }
      #endregion

      #region UsrMAWSGender
      [PXDBString(20)]
      [PXUIField(DisplayName="Gender")]
      public virtual string UsrMAWSGender { get; set; }
      public abstract class usrMAWSGender : IBqlField { }
      #endregion

      #region UsrMAWSName
      [PXDBString(20)]
      [PXUIField(DisplayName="Name")]
      public virtual string UsrMAWSName { get; set; }
      public abstract class usrMAWSName : IBqlField { }
      #endregion

      #region UsrMAWSWheelChairBrand
      [PXDBString(20)]
      [PXUIField(DisplayName="Wheel Chair Brand")]
      public virtual string UsrMAWSWheelChairBrand { get; set; }
      public abstract class usrMAWSWheelChairBrand : IBqlField { }
      #endregion

      #region UsrMAWSWheelChairDetail
      [PXDBString(50)]
      [PXUIField(DisplayName="Wheel Chair Details")]
      public virtual string UsrMAWSWheelChairDetail { get; set; }
      public abstract class usrMAWSWheelChairDetail : IBqlField { }
      #endregion

      #region UsrMAWSDisabilityType
      [PXDBString(50)]
      [PXUIField(DisplayName="Disability Type")]
      public virtual string UsrMAWSDisabilityType { get; set; }
      public abstract class usrMAWSDisabilityType : IBqlField { }
      #endregion
  }
}