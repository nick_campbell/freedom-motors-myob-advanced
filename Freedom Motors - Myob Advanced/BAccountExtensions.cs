using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using PX.TM;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace PX.Objects.CR
{
  public class BAccountExt : PXCacheExtension<PX.Objects.CR.BAccount>
  {
      #region UsrMAWSJobNumber
      [PXDBString(50)]
      [PXUIField(DisplayName="Job Number")]
      public virtual string UsrMAWSJobNumber { get; set; }
      public abstract class usrMAWSJobNumber : IBqlField { }
      #endregion

      #region UsrMAWSColor
      [PXDBString(50)]
      [PXUIField(DisplayName="Color")]
      public virtual string UsrMAWSColor { get; set; }
      public abstract class usrMAWSColor : IBqlField { }
      #endregion

      #region UsrMAWSMake
      [PXDBString(50)]
      [PXUIField(DisplayName="Make")]
      public virtual string UsrMAWSMake { get; set; }
      public abstract class usrMAWSMake : IBqlField { }
      #endregion

      #region UsrMAWSRegistration
      [PXDBString(50)]
      [PXUIField(DisplayName="Registration")]
      public virtual string UsrMAWSRegistration { get; set; }
      public abstract class usrMAWSRegistration : IBqlField { }
      #endregion

      #region UsrMAWSVIN
      [PXDBString(50)]
      [PXUIField(DisplayName="VIN")]
      public virtual string UsrMAWSVIN { get; set; }
      public abstract class usrMAWSVIN : IBqlField { }
      #endregion

      #region UsrMAWSEngineNumber
      [PXDBString(50)]
      [PXUIField(DisplayName="Engine Number")]
      public virtual string UsrMAWSEngineNumber { get; set; }
      public abstract class usrMAWSEngineNumber : IBqlField { }
      #endregion

      #region UsrMAWSDeliveryDate
      [PXDBDate]
      [PXUIField(DisplayName="Delivery Date")]
      public virtual DateTime? UsrMAWSDeliveryDate { get; set; }
      public abstract class usrMAWSDeliveryDate : IBqlField { }
      #endregion

      #region UsrMAWSOption1
      [PXDBString(100)]
      [PXUIField(DisplayName="Option 1")]
      public virtual string UsrMAWSOption1 { get; set; }
      public abstract class usrMAWSOption1 : IBqlField { }
      #endregion

      #region UsrMAWSOption2
      [PXDBString(100)]
      [PXUIField(DisplayName="Option 2")]
      public virtual string UsrMAWSOption2 { get; set; }
      public abstract class usrMAWSOption2 : IBqlField { }
      #endregion

      #region UsrMAWSOption3
      [PXDBString(100)]
      [PXUIField(DisplayName="Option 3")]
      public virtual string UsrMAWSOption3 { get; set; }
      public abstract class usrMAWSOption3 : IBqlField { }
      #endregion

      #region UsrMAWSOption4
      [PXDBString(100)]
      [PXUIField(DisplayName="Option 4")]
      public virtual string UsrMAWSOption4 { get; set; }
      public abstract class usrMAWSOption4 : IBqlField { }
      #endregion

      #region UsrMAWSOption5
      [PXDBString(100)]
      [PXUIField(DisplayName="Option 5")]
      public virtual string UsrMAWSOption5 { get; set; }
      public abstract class usrMAWSOption5 : IBqlField { }
      #endregion

      #region UsrMAWSTypeOfConversion
      [PXDBString(100)]
      [PXUIField(DisplayName="TypeOfConversion")]
      public virtual string UsrMAWSTypeOfConversion { get; set; }
      public abstract class usrMAWSTypeOfConversion : IBqlField { }
      #endregion

      #region UsrMAWABirthDate
      [PXDBDate]
      [PXUIField(DisplayName="Birth Date")]
      public virtual DateTime? UsrMAWABirthDate { get; set; }
      public abstract class usrMAWABirthDate : IBqlField { }
      #endregion

      #region UsrMAWSDisabilityType
      [PXDBString(50)]
      [PXUIField(DisplayName="Disability Type")]
      public virtual string UsrMAWSDisabilityType { get; set; }
      public abstract class usrMAWSDisabilityType : IBqlField { }
      #endregion

      #region UsrMAWSGender
      [PXDBString(10)]
      [PXUIField(DisplayName="Gender")]
      public virtual string UsrMAWSGender { get; set; }
      public abstract class usrMAWSGender : IBqlField { }
      #endregion

      #region UsrMAWSWheelChairBrand
      [PXDBString(20)]
      [PXUIField(DisplayName="WheelChair Brand")]
      public virtual string UsrMAWSWheelChairBrand { get; set; }
      public abstract class usrMAWSWheelChairBrand : IBqlField { }
      #endregion

      #region UsrMAWSWheelChairDetail
      [PXDBString(100)]
      [PXUIField(DisplayName="WheelChair Detail")]
      public virtual string UsrMAWSWheelChairDetail { get; set; }
      public abstract class usrMAWSWheelChairDetail : IBqlField { }
      #endregion

      #region UsrMAWSStatus
      [PXDBString(100)]
      [PXUIField(DisplayName="Freedom Status")]
      public virtual string UsrMAWSStatus { get; set; }
      public abstract class usrMAWSStatus : IBqlField { }
      #endregion

      #region UsrMAWSReferr
      [PXDBString(100)]
      [PXUIField(DisplayName="Referred By")]
      public virtual string UsrMAWSReferr { get; set; }
      public abstract class usrMAWSReferr : IBqlField { }
      #endregion

      #region UsrMAWSChildren
      [PXDBString(50)]
      [PXUIField(DisplayName="Children")]
      public virtual string UsrMAWSChildren { get; set; }
      public abstract class usrMAWSChildren : IBqlField { }
      #endregion

      #region UsrMAWSInterest
      [PXDBString(100)]
      [PXUIField(DisplayName="Interest")]
      public virtual string UsrMAWSInterest { get; set; }
      public abstract class usrMAWSInterest : IBqlField { }
      #endregion

      #region UsrMAWSModel
      [PXDBString(50)]
      [PXUIField(DisplayName="Model")]
      public virtual string UsrMAWSModel { get; set; }
      public abstract class usrMAWSModel : IBqlField { }
      #endregion

      #region UsrMAWSName
      [PXDBString(20)]
      [PXUIField(DisplayName="Name")]
      public virtual string UsrMAWSName { get; set; }
      public abstract class usrMAWSName : IBqlField { }
      #endregion

      #region UsrMAWSRestrictGroup
      [PXDBString(20)]
      [PXUIField(DisplayName="Restrict Group")]

      public virtual string UsrMAWSRestrictGroup { get; set; }
      public abstract class usrMAWSRestrictGroup : IBqlField { }
      #endregion
  }
}