using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.SO;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.CR
{
  public class CROpportunityProductsExt : PXCacheExtension<PX.Objects.CR.CROpportunityProducts>
  {
      #region UsrMAWSTransDesc
      [PXDBString(100)]
      [PXUIField(DisplayName="Custom Description(100)")]
      public virtual string UsrMAWSTransDesc { get; set; }
      public abstract class usrMAWSTransDesc : IBqlField { }
      #endregion
  }
}