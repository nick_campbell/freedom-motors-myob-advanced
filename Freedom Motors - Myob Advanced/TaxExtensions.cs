using PX.Data;
using PX.Objects.AP;
using PX.Objects.BQLConstants;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.TX.Descriptor;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.TX
{
  public class TaxExt : PXCacheExtension<PX.Objects.TX.Tax>
  {
      #region UsrMAWSATOCode
      [PXDBString(50)]
      [PXUIField(DisplayName="ATO Code")]

      public virtual string UsrMAWSATOCode { get; set; }
      public abstract class usrMAWSATOCode : IBqlField { }
      #endregion

      #region UsrMAWSATOCodeDesc
      [PXDBString(100)]
      [PXUIField(DisplayName="ATOCode Desc")]

      public virtual string UsrMAWSATOCodeDesc { get; set; }
      public abstract class usrMAWSATOCodeDesc : IBqlField { }
      #endregion
  }
}